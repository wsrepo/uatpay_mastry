<?php include('header.php'); ?>

<div class="wf-section"><div class="container hide_edg w-container"><div class="columns w-row"><div class="w-col w-col-11"><h2 class="title_reg">Register your account</h2></div><div class="w-col w-col-1"></div></div><div class="div-block-2"><img src="https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner.png" loading="lazy" sizes="(max-width: 479px) 90vw, (max-width: 767px) 94vw, (max-width: 1439px) 95vw, 1161px" srcset="https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-500.png 500w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-800.png 800w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-1080.png 1080w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner.png 1272w" alt="" class="image-2"></div></div></div>



<div class="container-default-1209px w-container">
	<div class="courses-hero-wrapper" style="align-items:baseline!important;">
		<div class="split-content courses">
			<h1 class="special-2 courses">Register</h1>
			<p class="paragraph courses">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra tristique laoreet ut elementum cras cursus. Morbi morbi at diam.</p><a href="https://www.mastry.io/courses" class="button-primary large w-button">View courses</a>
		</div>

		
		<div class="card contact-us no_mrg reg-form_wrapper"><div class="top-content contact-us"><div class="split-content contact-us-left head_form"></div></div><div class="w-form">
		
		<form id="wf-form-Teacher-Register" name="wf-form-Teacher-Register" data-name="Teacher Register" class="teacher-register">
		<div class="w-layout-grid contact-us-form-grid"><div class="input-wrapper"><label for="fname" class="field-label-6">First Name *</label>
		<input type="text" class="input w-input" maxlength="256" name="fname" data-name="fname" placeholder="Your first name" id="fname" required=""></div><div class="input-wrapper"><label for="lname" class="field-label-5">Last Name *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="lname" data-name="lname" placeholder="Your last name" id="lname" required=""></div><div class="input-wrapper"><label for="email-5" class="field-label-4">Email Address *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="email" data-name="email" placeholder="Your email address" id="email-5" required=""></div><div class="input-wrapper"><label for="phone-2" class="field-label-3">Phone Number *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="pass" data-name="pass" placeholder="Put a password" id="pass"></div></div><div class="input-wrapper new_wrap add_mar"><label for="linkedin" class="field-label-2">Password*<br></label>
		
		<input type="text" class="input w-input" maxlength="256" name="cpass" data-name="cpass" placeholder="Repeat password" id="cpass" required=""></div><div class="input-wrapper new_wrap add_mar"><label for="youtube" class="field-label-2">Repeat password<br></label>
		
		<input type="text" class="input w-input" maxlength="256" name="youtube" data-name="youtube" placeholder="YouTube link of you speaking or teaching" id="youtube"></div><div class="input-wrapper word-area"><label for="credential" class="field-label-8">Your Credentials to be a Teacher *</label>
		
		
		
		<input type="submit" value="Register" data-wait="Please wait..." class="button-primary large w-button"><input type="hidden" name="source" id="source" value="https://www.mastry.io/"></form><div class="success-message contact-us w-form-done"><img src="https://assets.website-files.com/60f95373e32d780647346711/6103e452610bdd5aac53e313_success.png" loading="lazy" alt=""></div></div></div></div>
		
		</div>
		
		<img src="images/6102bfc7ef1edc521bccd02f_60f95373e32d78692334676c_circle-shape-courses-01-academy-template.svg" alt="" class="circle-shape-courses _3">

	</div>
</div>



<?php include('footer.php'); ?>









