<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>.wf-force-outline-none[tabindex="-1"]:focus{outline:none;}</style>
<title>Mastry | User panel</title>

<link href="./css/ws-mastry.b16e736ba.min.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/jQuery.session.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-auth.js"></script>
</head>

<body>
<?php if(!isset($_SESSION)){ session_start();}?>

<?php $_SESSION['region'] = 'US';
if(isset($_REQUEST) && sizeof($_REQUEST) > 2){
    //$_SESSION['region'] = $_REQUEST['region'];    
    $_SESSION['item']['course_name'] = $_REQUEST['courseName'];
    $_SESSION['item']['course_id'] = $_REQUEST['courseID'];
    $_SESSION['item']['course_indian_price'] = $_REQUEST['courseIndianPrice'];
    $_SESSION['item']['course_indian_compare_price'] = $_REQUEST['courseIndianComparePrice'];
    $_SESSION['item']['course_us_price'] = $_REQUEST['courseUSPrice'];
    $_SESSION['item']['course_us_compare_price'] = $_REQUEST['courseUSComparePrice'];
    $_SESSION['item']['topic'] = $_REQUEST['topic'];
    $_SESSION['item']['duration'] = $_REQUEST['duration'];
    $_SESSION['item']['videos'] = $_REQUEST['videos'];
    $_SESSION['item']['files'] = $_REQUEST['files'];
    $_SESSION['item']['razorpay_plan_id'] = $_REQUEST['razorpayPlanID'];
    $_SESSION['item']['stripe_plan_id'] = $_REQUEST['stripePlanID'];
    $_SESSION['item']['teacher_id'] = $_REQUEST['teacherID'];
}?>
<?php //print_r($_REQUEST);?>

<div class="page-wrapper">
<div data-collapse="medium" data-animation="default" data-duration="1000" data-easing="ease-out-expo" data-easing2="ease-out-expo" data-w-id="483cd0ea-0ffd-1df5-02c0-5a28799302cc" role="banner" class="header w-nav"><div class="container-default-1209px w-container"><div class="header-wrapper"><div data-w-id="483cd0ea-0ffd-1df5-02c0-5a28799302cf" class="split-content header-left" style="transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d; opacity: 1;"><a href="https://www.mastry.io/" class="brand w-nav-brand" aria-label="home"><img src="./Courses - Academy - Webflow HTML Website Template_files/6101622f323d5b090c970ed7_image_2021_07_28T13_55_32_681Z.png" sizes="148px" srcset="https://assets.website-files.com/60f95373e32d780647346711/6101622f323d5b090c970ed7_image_2021_07_28T13_55_32_681Z-p-500.png 500w, https://assets.website-files.com/60f95373e32d780647346711/6101622f323d5b090c970ed7_image_2021_07_28T13_55_32_681Z.png 528w" alt="" class="header-logo"><div id="countrycode" class="global-icon">IN</div></a><nav role="navigation" class="nav-menu w-nav-menu"><a href="https://www.mastry.io/" class="nav-link">Home</a><a href="https://www.mastry.io/register" class="nav-link">Apply to Teach</a><a href="https://www.mastry.io/about-us" class="nav-link">About Us</a><a href="https://www.mastry.io/teachers" class="nav-link hide">Teachers</a><a href="https://www.mastry.io/courses" aria-current="page" class="nav-link hide w--current">Courses</a><div data-hover="" data-delay="0" data-w-id="8d41c972-856f-446c-3022-9951bcba0af8" class="header-dropdown w-dropdown" style="max-width: 1209px;"><div class="header-dropdown-toggle w-dropdown-toggle" id="w-dropdown-toggle-0" aria-controls="w-dropdown-list-0" aria-haspopup="menu" aria-expanded="false" role="button" tabindex="0"></div><nav class="dropdown-list w-dropdown-list" style="display: none; height: 0px; transform: translate3d(0px, 0px, 0px) scale3d(0.7, 0.7, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d; opacity: 0;" id="w-dropdown-list-0" aria-labelledby="w-dropdown-toggle-0"><div class="menu-grid-wrapper"><div class="w-layout-grid menu-grid"><div><h4 class="mega-menu-title hidden-mobile">Pages</h4><div class="menu-2-columns"><div class="mega-menu-column-1"><a href="https://www.mastry.io/" class="mega-menu-link" tabindex="0">Home</a><a href="https://www.mastry.io/about-us" class="mega-menu-link" tabindex="0">About</a><a href="https://www.mastry.io/courses" aria-current="page" class="mega-menu-link w--current" tabindex="0">Courses</a><a href="https://academytemplate.webflow.io/product/brand-identity-design-for-marketers" class="mega-menu-link" tabindex="0">Individual Course</a><a href="https://www.mastry.io/course/brand-identity-design-for-marketers" class="mega-menu-link" tabindex="0">Purchased Course</a></div><div class="mega-menu-column-3"><a href="https://www.mastry.io/blog" class="mega-menu-link" tabindex="0">Blog</a><a href="https://academytemplate.webflow.io/post/8-great-design-trends-that-are-coming-back" class="mega-menu-link" tabindex="0">Blog Post</a><a href="https://www.mastry.io/events" class="mega-menu-link" tabindex="0">Events</a><a href="https://academytemplate.webflow.io/event/brand-identity-design-qa-with-sophie-moore" class="mega-menu-link" tabindex="0">Individual Event</a><a href="https://www.mastry.io/teachers" class="mega-menu-link" tabindex="0">Teachers</a></div><div class="mega-menu-column-3"><a href="https://academytemplate.webflow.io/teacher/john-carter" class="mega-menu-link" tabindex="0">Individual Teacher</a><a href="https://www.mastry.io/contact-us" class="mega-menu-link" tabindex="0">Contact</a></div></div></div><div class="mega-menu-column-4"><h4 class="mega-menu-title">Utility Pages</h4><a href="https://www.mastry.io/style-guide" class="mega-menu-link" tabindex="0">Styleguide</a><a href="https://academytemplate.webflow.io/404" class="mega-menu-link" tabindex="0">404 Not Found</a><a href="https://academytemplate.webflow.io/401" class="mega-menu-link" tabindex="0">Password</a><a href="https://www.mastry.io/licenses" class="mega-menu-link" tabindex="0">Licenses</a><a href="https://www.mastry.io/start-here" class="mega-menu-link" tabindex="0">Start Here</a><a href="https://www.mastry.io/changelog" class="mega-menu-link" tabindex="0">Changelog</a></div></div></div></nav></div><a href="https://www.mastry.io/courses" aria-current="page" class="nav-link hide w--current">Courses</a><a href="https://www.mastry.io/blog" class="nav-link hide">Blog</a><a href="https://www.mastry.io/faq" class="nav-link">FAQ</a><a href="https://www.mastry.io/contact-us" class="nav-link hide">Contact</a></nav></div>

<?php if(empty($_SESSION['sessionID'])){?>

<div id="loginRegister"><nav role="navigation" class="nav-menu w-nav-menu"><a href="login.php" class="nav-link">Login</a></nav></div>

<?php } else {?>

<div id="userProfile"><nav role="navigation" class="nav-menu w-nav-menu"><a href="logout.php" class="nav-link">Logout</a></nav><nav role="navigation" class="nav-menu w-nav-menu"><a href="checkout.php" class="nav-link">Checkout</a></nav></div>

<?php }?>

<div class="spacer header-right"></div><a href="https://www.mastry.io/courses" class="button-primary header-button w-button">APPLY To TEACH</a><div data-w-id="63a3d12c-d009-7b48-0e75-aa6a4e6df1c4" class="menu-button w-nav-button" style="-webkit-user-select: text;" aria-label="menu" role="button" tabindex="0" aria-controls="w-nav-overlay-0" aria-haspopup="menu" aria-expanded="false"></div></div></div></div><div class="w-nav-overlay" data-wf-ignore="" id="w-nav-overlay-0"></div></div>