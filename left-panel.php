
<?php 
if(!isset($_SESSION)){ session_start();}
if(isset($_SESSION['item'])){?>
			<h4>Order details</h4>
<div class="course-details">

			
			<h4 class="title"><?php echo $_SESSION['item']['course_name'];?></h4>

			<?php if($_SESSION['region'] == 'IN'){?>
			
				<div class="course-detail-wrapper"><div class="course-detail-text">Subscription Price:&nbsp;</div><div class="course-detail-text strong">₹ <?php echo $_SESSION['item']['course_indian_price']; ?> INR</div></div>
				<div class="spacer course-details"></div>
				<div class="course-detail-wrapper"><div class="course-detail-text">Old Price:&nbsp;</div><div class="course-detail-text strong">₹ <?php echo $_SESSION['item']['course_indian_compare_price']; ?> INR</div></div>
				<div class="spacer course-details"></div>

			<?php }else{?>

				<div class="course-detail-wrapper"><div class="course-detail-text">Subscription Price:&nbsp;</div><div class="course-detail-text strong">$ <?php echo $_SESSION['item']['course_us_price']; ?> USD</div></div>
				<div class="spacer course-details"></div>
				<div class="course-detail-wrapper"><div class="course-detail-text">Old Price:&nbsp;</div><div class="course-detail-text strong"><?php echo $_SESSION['item']['course_us_compare_price']; ?></div></div>
				<div class="spacer course-details"></div>

			<?php }?>
			
			<div class="level-wrapper"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079f4126820deffc3349ca_topic%20(1).svg" loading="lazy" width="20" alt="" class="image-11"><div class="course-detail-text">Topic:&nbsp;</div><div class="w-dyn-list"><div role="list" class="levels-list w-dyn-items"><div role="listitem" class="level-text-wrapper w-dyn-item"><a href="/category/capital-markets" class="course-detail-text level"><?php echo $_SESSION['item']['topic']; ?></a></div></div></div></div></div><div class="spacer course-details"></div><div class="course-detail-wrapper"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079a52da023d7f14914a91_clock.svg" alt="" class="course-detail-icon standard-size"><div class="course-detail-text">Duration:&nbsp;</div><div class="course-detail-text strong"><?php echo $_SESSION['item']['duration']; ?></div></div><div class="spacer course-details"></div><div class="course-detail-wrapper"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079a6a1d4d3effe0437a32_play.svg" width="26" height="26" alt="" class="course-detail-icon"><div class="course-detail-text">Videos:&nbsp;</div><div class="course-detail-text strong"><?php echo $_SESSION['item']['videos']; ?></div></div><div class="spacer course-details"></div><div class="course-detail-wrapper"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079aa704a0cf1fbfb811c6_download.svg" height="26" alt="" class="course-detail-icon"><div class="course-detail-text">Downloadable Files:&nbsp;</div><div class="course-detail-text strong"><?php echo $_SESSION['item']['files']; ?></div></div><div class="spacer course-details"></div><div class="course-detail-wrapper"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079aed06e4d10cf5b5b878_life-time.svg" width="26" height="26" alt="" class="course-detail-icon"><div class="course-detail-text title-color">Lifetime Access&nbsp;</div></div><div class="spacer course-details"></div><div class="course-detail-wrapper align-top"><img src="https://assets.website-files.com/60f95373e32d780647346711/61079b17b1082cbb2b8d8273_device.svg" width="26" height="26" alt="" class="course-detail-icon"><div class="course-detail-text title-color">Access from any Computer, Tablet or Mobile
				
			
			</div></div>		



		<?php }else{echo 'Nothing found!';}?>	
		</div>