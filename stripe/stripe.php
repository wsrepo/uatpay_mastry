<button class="button-primary header-button w-button" id="stripe-demo">PAY NOW</button>

<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
var handler = StripeCheckout.configure({
  key: "pk_test_mgTQPYkox0t6B1WZdRm9oTBT",
  image: "https://assets.website-files.com/60f95373e32d780647346711/6101622f323d5b090c970ed7_image_2021_07_28T13_55_32_681Z-p-500.png",
  name: "<?php echo $_SESSION['item']['course_name'];?>",
  //description: "Pro Subscription ($29 per month)",
  panelLabel: "Subscribe",
  allowRememberMe: false
});

document.getElementById('stripe-demo').addEventListener('click', function(e) {
  handler.open();
  e.preventDefault();
});
</script>