<?php include('header.php'); 
if(!isset($_SESSION)){ session_start();}?>

<div class="wf-section"><div class="container hide_edg w-container"><div class="columns w-row"><div class="w-col w-col-11"><h2 class="title_reg"><span id="loginTxt" style="display:none;">Login</span><span id="registerTxt" style="display:none;">Register</span> to  your account</h2></div><div class="w-col w-col-1"></div></div><div class="div-block-2"><img src="https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner.png" loading="lazy" sizes="(max-width: 479px) 90vw, (max-width: 767px) 94vw, (max-width: 1439px) 95vw, 1161px" srcset="https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-500.png 500w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-800.png 800w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner-p-1080.png 1080w, https://assets.website-files.com/60f95373e32d780647346711/6103e1517b00b8c5c0617a45_registration-banner.png 1272w" alt="" class="image-2"></div></div></div>

<script>
$(document).ready(function(){
	$("#loginForm").show();
	$("#loginTxt").show();
});
function showRegisterBox(){
    $("#loginForm").toggle();
	$("#registerForm").toggle();
	$("#loginTxt").toggle();
	$("#registerTxt").toggle();
	$("#loading-image").hide();
}
function showLoginBox(){
    $("#loginForm").toggle();
	$("#registerForm").toggle();
	$("#loginTxt").toggle();
	$("#registerTxt").toggle();
	$("#loading-image").hide();
}
</script>
<script>
function signInCustom(token) {
  //var token = "token123";
  // [START auth_sign_in_custom]
  firebase.auth().signInWithCustomToken(token)
    .then((userCredential) => {
      // Signed in
      var user = userCredential.user;
	  
	  user
                  .getIdToken(true)
                  .then((idToken) => {
                    //var decoded = jwt_decode(idToken);
					console.log(idToken);
                    debugger;
                    console.log(`get token Success Execution time:`, new Date());
				  });
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
  // [END auth_sign_in_custom]
}
</script>



<script>
$(document).ready(function(){
//Login form
    $("#loginBtn").click(function(){ 
	var loginEmail = $("#email").val();	
	var loginPass = $("#password").val();
	
	if(loginEmail != '' && loginPass != ''){

	//Get auth Token
	var formData = {emailId:loginEmail, password:loginPass}; //Array 

	$.ajax({
		url : "https://us-central1-uate2monair.cloudfunctions.net/e2monair-uat-get-auth-token", // Url of backend (can be python, php, etc..)
		headers: {
			  "Content-Type": "application/json",
		},
		type: "POST", // data post type (can be get, post, put, delete)
		mode: 'cors',
		crossDomain: true,
		dataType: "json",
		processData: false,
		//data : formData, // data in json format
		//beforeSend: function(xhr){xhr.setRequestHeader('x-api-key', 'AIzaSyDGa2G93X25PKfohnCKIDeSesTm4Vq3atY');},
		data: JSON.stringify(formData),
		beforeSend: function() {
              $("#loading-image").show();
           },
		success: function(response, textStatus, jqXHR) {
			$("#loading-image").hide();
			console.log(response);
			//alert(response['customToken']);
			
			if('customToken' in response && response['customToken'] != ''){

/*var webflowAuth = {
  firebaseConfig: {
    apiKey: "AIzaSyAY-vCM15erTOg3zCaguwIWwfsuvvILD7w",
	authDomain: "uate2monair.firebaseapp.com",
	databaseURL: "https://uate2monair.firebaseio.com",
	projectId: "uate2monair",
	storageBucket: "uate2monair.appspot.com",
	messagingSenderId: "119219268154"
  }
};*/

var webflowAuth = { 
	firebaseConfig = {
	apiKey: "AIzaSyCJC9KV9eAtZgRdWnlULGpCsj1OyaDxBh0",
	authDomain: "testmastry.firebaseapp.com",
	projectId: "testmastry",
	storageBucket: "testmastry.appspot.com",
	messagingSenderId: "704768155139",
	appId: "1:704768155139:web:c3a46de1df90539017f836",
	measurementId: "G-5WPW3H5P5J"
   }
};

firebase.initializeApp(webflowAuth.firebaseConfig);
signInCustom(response['customToken']);

				//var token = response['customToken'];
				var userData = {
					sessionID: response['customToken'],
					fname: response['data']['FirstName'],
					lname: response['data']['LastName'],
					desig: response['data']['Designation'],
					company: response['data']['Company'],
					email: response['data']['Email'],
					phone: response['data']['Phone'],
					attendeeID: response['data']['AttendeeId'],
					clientID: response['data']['ClientId'],
				};

				$.ajax({
					type: "POST",
					url: "ajax.php",
					data: userData,
					dataType: "json",
					encode: true,
					}).done(function (data) {
					console.log(data);
					$("#success").show();
					//$("#wf-form-Student-Login").submit(); // Submit the form
				});
				event.preventDefault();
				//checking
			}
			else{
				$("#error").show();
			}
			//document.getElementById("demo").innerHTML = JSON.stringify(response);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
	}	
        
    });

	
		
	

//Register form
	$("#registerBtn").click(function(){ 
	var registerFname = $("#fname").val();	
	var registerLname = $("#lname").val();
	var registerEmail = $("#email").val();
	var registerPhone = $("#phone").val();
	var registerPass = $("#pass").val();
	var registerCpass = $("#cpass").val();
	
	//if(loginEmail != '' && loginPass != ''){

	var formData = {
		"key": {
			"instanceId": "OA_UAT",
			"clientId": "C1591205467009",
			"eventId": "E1617254416586"
		},
		"data": {
			"EdulifeId": '##',
			"FirstName": registerFname,
			"LastName": registerLname,
			"Email": registerEmail,
			"Password": registerPass
		}
	};

	$.ajax({
		url : "https://m4r9jvvpfl.execute-api.us-east-1.amazonaws.com/UAT/register-event-attendee-sync", // Url of backend (can be python, php, etc..)
		headers: {
			  "Content-Type": "application/json",
			  "x-api-key": "DACp6AMlLD3zPL3CXRCvU9hlGEierp7m6xdJLOlH",
		},
		type: "POST", // data post type (can be get, post, put, delete)
		mode: 'cors',
		crossDomain: true,
		dataType: "json",
		processData: false,
		//data : formData, // data in json format
		//beforeSend: function(xhr){xhr.setRequestHeader('x-api-key', 'AIzaSyDGa2G93X25PKfohnCKIDeSesTm4Vq3atY');},
		data: JSON.stringify(formData),
		beforeSend: function() {
              $("#loading-image").show();
           },
		success: function(response, textStatus, jqXHR) {
			$("#loading-image").hide();
			console.log(response);
			//alert(response['customToken']);			
			
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
	//}	
        
    });
});
</script>





<div class="container-default-1209px w-container">
	<div class="courses-hero-wrapper" style="align-items:baseline!important;">
		<div class="split-content courses" style="max-width:inherit!important;">
		
		<?php include('left-panel.php');?>

		
		<div class="card contact-us no_mrg reg-form_wrapper" id="loginForm" style="display:none;"><div class="w-form">
		
		<form method="post" action="checkout.php" id="wf-form-Student-Login" name="wf-form-Student-Login" data-name="Student Login" class="student-login">
		<div class="input-wrapper new_wrap add_mar"><label for="email" class="field-label-2">Email address*<br></label>
		
		<input type="email" value="" class="input w-input" maxlength="256" name="email" data-name="email" placeholder="Your registered email" id="email"></div><div class="input-wrapper new_wrap add_mar"><label for="password" class="field-label-2">Password<br></label>
		
		<input type="password" value="" class="input w-input" maxlength="256" name="password" data-name="password" placeholder="Put password" id="password"></div>
		
		<div class="error-message hide" id="error">You're not a registered user. <a href="javascript:showRegisterBox()">Register Now</a></div>
		<div id="loading-image" style="display:none;"><img src="images/loading.gif"></div>
		
		<!-- <input type="submit" value="Login" id="loginBtn" data-wait="Please wait..." class="button-primary large w-button"> -->
		<button type="button" id="loginBtn" class="button-primary large w-button">Login</button>
		</form></div></div>

		<div class="card contact-us no_mrg reg-form_wrapper" id="registerForm" style="display:none;">Back to <a href="javascript:showLoginBox()">Login</a><div class="w-form">
		
		<form id="registerForm" method="post" action="" class="teacher-register">
		<div class="w-layout-grid contact-us-form-grid"><div class="input-wrapper"><label for="fname" class="field-label-6">First Name *</label>
		<input type="text" class="input w-input" maxlength="256" name="fname" data-name="fname" placeholder="Your first name" id="fname" required=""></div><div class="input-wrapper"><label for="lname" class="field-label-5">Last Name *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="lname" data-name="lname" placeholder="Your last name" id="lname" required=""></div><div class="input-wrapper"><label for="email" class="field-label-4">Email Address *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="email" data-name="email" placeholder="Your email address" id="email-5" required=""></div><div class="input-wrapper"><label for="phone" class="field-label-3">Phone Number *</label>
		
		<input type="text" class="input w-input" maxlength="256" name="phone" data-name="phone" placeholder="Put your phone number" id="phone"></div></div><div class="input-wrapper new_wrap add_mar"><label for="password" class="field-label-2">Password*<br></label>
		
		<input type="password" class="input w-input" maxlength="256" name="pass" data-name="pass" placeholder="Choose password" id="pass" required=""></div><div class="input-wrapper new_wrap add_mar"><label for="cpassword" class="field-label-2">Repeat password*<br></label>
		
		<input type="password" class="input w-input" maxlength="256" name="cpass" data-name="cpass" placeholder="Repeat password" id="youtube"></div><div class="input-wrapper word-area">	

		<div class="success-message hide" id="success">Your registration was successful! <a href="javascript:showLoginBox()">Login Now</a>
		</div>
		<div id="loading-image" style="display:none;"><img src="images/loading.gif"></div>
		
		<button type="button" id="registerBtn" class="button-primary large w-button">Register</button>
		</form></div></div></div>

		



		
		</div>


		
		
		
		
		<img src="images/6102bfc7ef1edc521bccd02f_60f95373e32d78692334676c_circle-shape-courses-01-academy-template.svg" alt="" class="circle-shape-courses _3">

	</div>
</div>



<?php include('footer.php'); ?>









