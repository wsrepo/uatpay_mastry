<?php 
//Create subscription plan
    $key_id = 'rzp_test_vtA3FrVCRJx8zP';
    $key_secret = 'vs9o6hEWECKJ0rU0ei8FoIX3';
    $startTime = (time()+300);
    $endTime = $startTime + 30*24*3600;
    $amount = $_SESSION['item']['course_indian_price']*100;
    $currency_code = 'INR';

    function razorpay_create_plan($key_id, $key_secret, $amount, $currency_code){
        //Create plan for user
        $planReqdata = array(
            'period' => 'monthly',
            'interval' => 1,
            'item' => array(
                'name' => $_SESSION['item']['course_name'],
                //'description' => 'Description for the daily 1 plan',
                'amount' => $amount,
                'currency' => $currency_code                
            )
        );
        $planurl = 'https://api.razorpay.com/v1/plans';
        $palnparams = http_build_query($planReqdata);
        //cURL Request
        $planch = curl_init();
        //set the url, number of POST vars, POST planReqdata
        curl_setopt($planch, CURLOPT_URL, $planurl);
        curl_setopt($planch, CURLOPT_USERPWD, $key_id . ':' . $key_secret);
        curl_setopt($planch, CURLOPT_TIMEOUT, 60);
        curl_setopt($planch, CURLOPT_POST, 1);
        curl_setopt($planch, CURLOPT_POSTFIELDS, $palnparams);
        curl_setopt($planch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($planch, CURLOPT_SSL_VERIFYPEER, true);

        $planResult = curl_exec($planch);
        $planRes = json_decode($planResult);
    }
	
//echo '<br></br>';	

//Create subscription for user
        $subdata = array(
            'plan_id' => $_SESSION['item']['razorpay_plan_id'],
            'customer_notify' => 1,
            'total_count' => 12,
            'start_at' =>  $startTime, 
            'expire_by' => $endTime,             
            'addons' => array(
                array(
                'item' => array(
                    'name' => 'Delivery charges',
                    'amount' => $amount,
                    'currency' => $currency_code
                    )
                )
            )
        );
        $suburl = 'https://api.razorpay.com/v1/subscriptions';
        $subparams = http_build_query($subdata);
        //cURL Request
        $subch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($subch, CURLOPT_URL, $suburl);
        curl_setopt($subch, CURLOPT_USERPWD, $key_id . ':' . $key_secret);
        curl_setopt($subch, CURLOPT_TIMEOUT, 60);
        curl_setopt($subch, CURLOPT_POST, 1);
        curl_setopt($subch, CURLOPT_POSTFIELDS, $subparams);
        curl_setopt($subch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($subch, CURLOPT_SSL_VERIFYPEER, true);

        $subResult = curl_exec($subch);
        $subres = json_decode($subResult);
        //print_r($subres);
		

?>

<button id = "rzp-button1" class="button-primary header-button w-button">PAY NOW</button>
            <script src = "https://checkout.razorpay.com/v1/checkout.js"></script>
            <script>
            var options = {
                "key": "<?php echo $key_id; ?>",
                "subscription_id": "<?php echo $subres->id; ?>",
                "name": "<?php echo $_SESSION['item']['course_name'];?>",
                //"description": "Daily Test Plan",
                "image": "https://assets.website-files.com/60f95373e32d780647346711/6101622f323d5b090c970ed7_image_2021_07_28T13_55_32_681Z-p-500.png",
                "callback_url": "http://localhost/mastry/razorpay-success.php",
                "prefill": {
                "name": "<?php echo $_SESSION['user']['fname'];?>",
                "email": "<?php echo $_SESSION['user']['email'];?>",
                "contact": "<?php echo $_SESSION['user']['phone'];?>"
                },
                "notes": {
                //"note_key_1": "Tea. Earl Grey. Hot",
                //"note_key_2": "Make it so."
                },
                "theme": {
                "color": "#ff0081"
                }
            };
            var rzp1 = new Razorpay(options);
            document.getElementById('rzp-button1').onclick = function(e) {
            rzp1.open();
            e.preventDefault();
            }
            </script>