<?php
session_start();
require('config.php');
require('razorpay-php/Razorpay.php');
//session_start();

// Create the Razorpay Order

use Razorpay\Api\Api;

$api = new Api($keyId, $keySecret);

//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//
$startTime = (time()+300);
    $endTime = $startTime + 30*24*3600;
    $amount = $_SESSION['course_indian_price']*100;
    $currency_code = 'INR';

$subdata = array(
    'plan_id' => $_SESSION['razorpay_plan_id'],
    'customer_notify' => 1,
    'total_count' => 12,
    'start_at' =>  $startTime, 
    'expire_by' => $endTime,             
    'addons' => array(
        array(
        'item' => array(
            'name' => 'Delivery charges',
            'amount' => $amount,
            'currency' => $currency_code
            )
        )
    )
);

$razorpaySubs = $api->subscription->create($subdata);

$razorpaySubsId = $razorpaySubs['id'];

$_SESSION['razorpay_sub_id'] = $razorpaySubsId;

$displayAmount = $amount = $razorpaySubs['amount'];

if ($displayCurrency !== 'INR')
{
    $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
    $exchange = json_decode(file_get_contents($url), true);

    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
}

$checkout = 'manual';

if (isset($_GET['checkout']) and in_array($_GET['checkout'], ['automatic', 'manual'], true))
{
    $checkout = $_GET['checkout'];
}

$data = [
    "key"               => $keyId,
    "amount"            => $amount,
    "name"              => $_SESSION['fname'],
    //"description"       => "Tron Legacy",
    "image"             => "https://s29.postimg.org/r6dj1g85z/daft_punk.jpg",
    "prefill"           => [
    "name"              => $_SESSION['fname'],
    "email"             => $_SESSION['email'],
    "contact"           => $_SESSION['phone'],
    ],
    "notes"             => [
    "address"           => "Hello World",
    "merchant_order_id" => "12312321",
    ],
    "theme"             => [
    "color"             => "#F37254"
    ],
    "subscription_id"=> $razorpaySubsId,
];

if ($displayCurrency !== 'INR')
{
    $data['display_currency']  = $displayCurrency;
    $data['display_amount']    = $displayAmount;
}

$json = json_encode($data);

require("checkout/{$checkout}.php");
