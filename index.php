<?php include('header.php'); ?>


<div class="section courses wf-section">
<div class="container-default-1209px w-container">
	<div class="courses-hero-wrapper">
		<div class="split-content courses">
			<h1 class="special-2 courses">Courses</h1>
			<p class="paragraph courses">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Viverra tristique laoreet ut elementum cras cursus. Morbi morbi at diam.</p><a href="https://www.mastry.io/courses" class="button-primary large w-button">View courses</a>
		</div>

		<img src="images/6102bba28c5b2307478c8faa_courses-hero.jpg" alt="" class="image courses">
		<img src="images/6102bfc7ef1edc521bccd02f_60f95373e32d78692334676c_circle-shape-courses-01-academy-template.svg" alt="" class="circle-shape-courses _3">

	</div>
</div>
</div>


<?php include('footer.php'); ?>









