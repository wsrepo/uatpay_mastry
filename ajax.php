<?php session_start();

if (!empty($_POST['sessionID'])) {
    $_SESSION['sessionID'] = $_POST['sessionID'];
}

if (!empty($_POST['fname'])) {
    $_SESSION['user']['fname'] = $_POST['fname'];
}

if (!empty($_POST['lname'])) {
    $_SESSION['user']['lname'] = $_POST['lname'];
}

if (!empty($_POST['desig'])) {
    $_SESSION['user']['desig'] = $_POST['desig'];
}

if (!empty($_POST['company'])) {
    $_SESSION['user']['company'] = $_POST['company'];
}

if (!empty($_POST['email'])) {
    $_SESSION['user']['email'] = $_POST['email'];
}

if (!empty($_POST['phone'])) {
    $_SESSION['user']['phone'] = $_POST['phone'];
}

echo json_encode(1);
?>